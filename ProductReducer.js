export default (state = [], action) => {
    switch (action.type) {
        case 'EDIT_PRODUCT':
            return [...state,
            {
                productname: action.productname,
            }]
        case 'ADD_PRODUCT':
            return [...state,
            {
                productname: action.productname,
            }]
        case 'ADD_1PRODUCT':
            return state.filter((item, index) => { index !== action.index })
        default:
            return state
    }
}
import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import LoginPage from './LoginPage'
import ListPage from './ListPage'
import ProfilePage from './ProfilePage'
import EditProfilePage from './EditProfilePage'
import AddProduct from './AddProduct'
import ProductProfile from './ProductProfile'
import EditProduct from './EditProduct'

import { Provider } from 'react-redux'
import store from './AppStore'

class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <NativeRouter>
                    <Switch>
                        <Route exact path="/LoginPage" component={LoginPage} />
                        <Route exact path="/ListPage" component={ListPage} />
                        <Route exact path="/ProfilePage" component={ProfilePage} />
                        <Route exact path="/EditProfilePage" component={EditProfilePage} />
                        <Route exact path="/AddProduct" component={AddProduct} />
                        <Route exact path="/ProductProfile" component={ProductProfile} />
                        <Route exact path="/EditProduct" component={EditProduct} />
                        <Redirect to="/LoginPage" />
                    </Switch>
                </NativeRouter>
            </Provider>
        )
    }
}
export default Router
import { createStore, combineReducers } from 'redux';

import AccountReducer from './AccountReducer';
import ProductReducer from './ProductReducer';

const reducers = combineReducers({
    account : AccountReducer,
    product : ProductReducer,
  
})

const store = createStore (reducers);
export default store;
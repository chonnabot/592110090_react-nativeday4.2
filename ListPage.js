import React from 'react';
import { StyleSheet, Text, View, Modal, TouchableOpacity, ScrollView, Image } from 'react-native';
import { connect } from 'react-redux'
class ListPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isShow: false,
            message: " ",
            username: '',
            productname: '',
        };
    }
    // onShowModal = (message) => {
    //     this.setState({ isShow: true, message })
    // }
    // onHideModal = () => {
    //     this.setState({ isShow: false, })
    // }
    goToScreen3 = () => {
        return this.props.history.push('/ProfilePage')
    }
    goToScreen5 = () => {
        return this.props.history.push('/AddProduct')
    }
    goToScreen6 = () => {
        return this.props.history.push('/ProductProfile')
    }
    render() {
        const { account, product} = this.props
        console.log(account)
        console.log(product)
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.headerBox1}>
                        <Text style={styles.ham}></Text>
                        <Text style={styles.ham}></Text>
                        <Text style={styles.ham}></Text>

                    </View>
                    <View style={styles.headerBox2}>
                        <View style={styles.headerBox1}>
                            <Text style={styles.Text}>List</Text>
                        </View>
                    </View>
                    <View style={styles.headerBox3}>
                        <View style={styles.headerBox1}>
                            <Text style={styles.Text}>X</Text>
                        </View>
                    </View>
                </View>

                <ScrollView style={styles.body}>
                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <TouchableOpacity onPress={this.goToScreen6}>
                                <Image
                                    style={styles.image}
                                    source={{ uri: 'https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/50416171_2144023502324187_7446495404520112128_n.jpg?_nc_cat=111&_nc_ht=scontent-kut2-2.xx&oh=3c6f5d3ea9132af86cc030014e0f5681&oe=5CF6C7A7' }}
                                />
                                <Text style={styles.Text}
                                >{product[product.length - 1].productname}</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.box2} >
                            <TouchableOpacity onPress={this.goToScreen6}>
                                <Image
                                    style={styles.image}
                                    source={{ uri: 'https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/50416171_2144023502324187_7446495404520112128_n.jpg?_nc_cat=111&_nc_ht=scontent-kut2-2.xx&oh=3c6f5d3ea9132af86cc030014e0f5681&oe=5CF6C7A7' }}
                                />
                                <Text style={styles.Text}
                                    onPress={this.goToScreen6}
                                >{product[product.length - 1].productname}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={styles.row}>
                        <View style={styles.box1} >
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                    onPress={this.goToScreen6}
                                >Lorme3</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.box2} >
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                    onPress={this.goToScreen6}
                                >Lorme4</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1} >
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                    onPress={this.goToScreen6}
                                >Lorme5</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.box2} >
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                    onPress={this.goToScreen6}
                                >Lorme6</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1} >
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                    onPress={this.goToScreen6}
                                >Lorme7</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.box2}>
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                    onPress={this.goToScreen6}
                                >Lorme8</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                    onPress={this.goToScreen6}
                                >Lorme9</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.box2} >
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                    onPress={this.goToScreen6}
                                >Lorme10</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                    onPress={this.goToScreen6}
                                >Lorme11</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.box2}>
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                    onPress={this.goToScreen6}
                                >Lorme12</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>

                <View style={styles.footer}>
                    <View style={styles.footerBox1}>
                        <Text style={styles.Text}>L</Text>
                    </View>
                    <View style={styles.footerBox2}>
                        <TouchableOpacity>
                            <Text style={styles.Text} onPress={this.goToScreen5}>Add</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.footerBox3}>
                        <Text style={styles.Text} onPress={this.goToScreen3}>P</Text>
                    </View>
                </View>

            </View>
        );                                                  //marginHorizontal: -8
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1

    },
    Text: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
        textAlign: 'center',
    },
    header: {
        backgroundColor: 'white',
        flexDirection: 'row',
        flex: 0.1,
    },
    headerBox1: {
        backgroundColor: '#00BFFF',
        flex: 1,
        margin: 2,
        marginLeft: -1,
        justifyContent: 'center',  //เลื่อนลงตาม row บนมาล่าง
        alignItems: 'center', // เลื่อน ซ้ายไปขวา
    },
    headerBox2: {
        backgroundColor: '#00BFFF',
        flex: 3,
        margin: 2,
    },
    headerBox3: {
        backgroundColor: '#00BFFF',
        flex: 1,
        margin: 2,
        marginRight: -1,
        alignItems: 'center',
    },
    body: {
        backgroundColor: '#666',
        flex: 10,
        margin: 2,
        // justifyContent: 'center',  //เลื่อนลงตาม row บนมาล่าง
        // alignItems: 'center', // เลื่อน ซ้ายไปขวา
        marginHorizontal: -1,
    },
    bodyScrollView: {
        backgroundColor: 'red',
        flexDirection: 'row',
        margin: 2,
        marginHorizontal: -1,
    },
    row: {
        backgroundColor: '#666',
        flexDirection: 'row',
        flex: 1,
    },
    box1: {
        backgroundColor: '#00BFFF',
        flex: 1,
        margin: 14,
        flexDirection: 'column',
        alignItems: 'center',
        width: 200,
        height: 230,
    },
    box2: {
        backgroundColor: '#00BFFF',
        flex: 1,
        margin: 14,
        flexDirection: 'column',
        alignItems: 'center',
        width: 200,
        height: 230,
    },
    modal: {
        backgroundColor: "rgba(192,192,192,0.6)",
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        margin: 30,
    },
    footer: {
        backgroundColor: 'white',
        flexDirection: 'row',
        flex: 0.1,
        margin: 1,
    },
    footerBox1: {
        backgroundColor: '#00BFFF',
        flex: 1,
        margin: 2,
        marginLeft: -1,
        alignItems: 'center',
    },
    footerBox2: {
        backgroundColor: '#00BFFF',
        flex: 3,
        margin: 2,
        alignItems: 'center',
    },
    footerBox3: {
        backgroundColor: '#00BFFF',
        flex: 1,
        margin: 2,
        marginRight: -1,
        alignItems: 'center',
    },
    ham: {
        width: 35,
        height: 3.5,
        backgroundColor: 'white',
        margin: 2,
    },
    image: {
        backgroundColor: 'white',
        width: 150,
        height: 150,
        margin: 10


    },


});

const mapStateToProps = (state) => {
    return {
        account: state.account,
        product: state.product
    }
}

export default connect(mapStateToProps)(ListPage)



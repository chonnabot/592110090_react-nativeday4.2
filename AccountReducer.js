export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_ACCOUNT':
            return [...state,
            {
                username: action.username,
                firstname: action.firstname,
                lastname: action.lastname
            }]
        case 'EDIT_ACCOUNT':
            return state.filter((item, index) => { index !== action.index })
        default:
            return state
    }
}